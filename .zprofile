# exporting paths
export PATH="$HOME/.local/bin:$PATH"
export PATH="$PATH":"$HOME/.pub-cache/bin"

# Auto run commands
#fc-cache -f &

# qt5 app themes
export QT_QPA_PLATFORMTHEME=qt5ct

# Setting up the editor
export EDITOR=/usr/bin/nvim

# Setting up the Pager
export PAGER='most'

# Setting up Ibus for Unicode support
export GTK_IM_MODULE="ibus"
export QT_IM_MODULE="ibus"
export XMODIFIERS="@im=ibus"
ibus-daemon -drx

# Telegram support for ibus
# QT_IM_MODULE=ibus /usr/bin/telegram-desktop
# Don't uncomment this line, it makes x11 startx with Telegram,
# Still need to find a workaround to makeske ibus function with Telegram.

# Changing Starship default config location
export STARSHIP_CONFIG=~/.config/starship/config.toml

# Changing Starship default cache file
export STARSHIP_CACHE=~/.config/starship/cache

# Fix for some Java programs not opening or opening with window issues
#wmname LG3D

# Fixing IntelliJ path
export IDEA_JDK=/usr/lib/jvm/jdk-jetbrains
