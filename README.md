## What is this repo?

This is a place where I will keep my dotfiles secured.
Those files will be updated as I modify or add new packages on my system.

### What (Free) Software you use?

Well, as a Free Software enthusiast, I try to use as much free software (often called Open Source or FOSS as in "Free and Open Source Software"), which is listed on the table bellow:

|PROGRAM                                        |NAME                                                                                                                               |
|------                                         |------                                                                                                                             |
|Distribution                                   |[Artix Linux](https://artixlinux.org/ "Systemd-free OS")                                                                           |
|System Init                                    |[OpenRC System Init](https://github.com/OpenRC/openrc)                                                                             |
|Display Server                                 |[X11 (Xorg)](https://gitlab.freedesktop.org/xorg)                                                                                  |
|Video Driver                                   |[modesetting Video Driver](https://man.archlinux.org/man/modesetting.4 "Using AMD Graphics")                                       |
|Desktop Manager                                | **None** I use *startx*                                                                                                           |
|Window Manager                                 |[BSPWM](https://github.com/baskerville/bspwm "Binary Space Partitioning Window Manager")                                           |
|Bar/Panel                                      |[Polybar](https://github.com/polybar/polybar "Lemon Bar is just too difficult")                                                    |
|Program Launcher                               |[Rofi](https://github.com/davatorium/rofi)                                                                                         |
|Wallpaper Set                                  |[Feh](https://github.com/derf/feh)                                                                                                 |
|Web Browser                                    |[Brave](https://brave.com/)                                                                                                        |
|Icon Theme                                     |[Papirus Icon Theme](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)                                                 |
|Code Editor                                    |[NeoVim](https://github.com/neovim/neovim "No Electron Garbage on my Computer")                                                    |
|Terminal Font                                  |[JetBrainsMono Nerd Font](https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/JetBrainsMono.zip "JetBrains, but cool")|
|Shell                                          |[ZSH](https://zsh.sourceforge.io/)                                                                                                 |
|Terminal Emulator                              |[Alacritty](https://github.com/alacritty/alacritty)                                                                                |
|Prompt                                         |[Startship](https://starship.rs/)                                                                                                  |
|Document Viewer                                |[Zathura](https://pwmt.org/projects/zathura/)                                                                                      |
|Music Server                                   |[mpd](https://github.com/MusicPlayerDaemon/MPD)                                                                                    |
|Music Client                                   |[ncmpcpp](https://github.com/ncmpcpp/ncmpcpp)                                                                                      |
|GTK Theme                                      |[Orchis GTK Theme](https://github.com/vinceliuice/Orchis-theme)                                                                    |
|Qt Theme                                       |[Orchis Kvantum git](https://github.com/vinceliuice/Orchis-kde "Use Kvantum to apply the theme")                                   |
|Lockscreen                                     |[Betterlockscreen](https://github.com/betterlockscreen/betterlockscreen)                                                           |
|Sound Server                                   |[PulseAudio](https://www.freedesktop.org/wiki/Software/PulseAudio/ "PipeWire... one day...")                                       |
|Sound Mixer                                    |[AlsaMixer](https://github.com/alsa-project/alsa-utils "You need alsa-utils installed")                                            |
|Polkit Frontend                                |[GNOME Polkit](https://archlinux.org/packages/?name=polkit-gnome)                                                                  |
|CLI File Manager                               |[Ranger](https://github.com/ranger/ranger "Not the fastest, but it's still a cool thing")                                          |
|Image Viewer                                   |[feh](https://wiki.archlinux.org/title/feh)                                                                                        |
|Screenshooter                                  |[maim](https://github.com/naelstrof/maim "Better than I thought it'd be")                                                          |

## Preview

### My Desktop

![A Screenshot of my Desktop](https://gitlab.com/keven-mate/dotfiles/-/raw/main/source/Screenshots/Dark%20Monochrome/2022-01-30-22:19:59.png "Humm, nice desktop you got there")*Get the Wallpaper* [here](https://gitlab.com/keven-mate/dotfiles/-/blob/main/source/Wallpapers/Saturn-Blck-White.jpg)

### Some terminals

![A Screenshot of some terminals](https://gitlab.com/keven-mate/dotfiles/-/raw/main/source/Screenshots/Dark%20Monochrome/2022-01-30-22:19:50.png "Sweet")

### Some more terminals

![A Screenshot of some more terminals](https://gitlab.com/keven-mate/dotfiles/-/raw/main/source/Screenshots/Dark%20Monochrome/2022-01-30-21:23:04.png "All this work because xf86-video-intel Driver sucks a hell lot")

### Rofi

![A screenshot of rofi, my program launcher](https://gitlab.com/keven-mate/dotfiles/-/raw/main/source/Screenshots/Dark%20Monochrome/2022-01-30-22:52:55.png "Okay, not the best config, but still a lot handy")

### Lockscreen

![A Screenshot of my lockscreen](https://gitlab.com/keven-mate/dotfiles/-/raw/main/source/Screenshots/Dark%20Monochrome/Old-Screenshots/Lockscreen.png "Phat Wallpaper")*Get Wallpaper* [here](https://gitlab.com/keven-mate/dotfiles/-/blob/main/source/Wallpapers/Looking-At-Planets.png)



## Keybindings
I will only be listing keybindings added by me, keybindings I consider important, or keybindigs that I modified, as for most they're still the default for SXHKD (**Simple X Hotkey Daemon**)

**Note**: The *super* key means *windows* key, or *alt* if you manually modified it, or you're using Debian Buster (I don't know if SXHKD keybindings were updated on Debian).

- **Super + Enter**         - Open Terminal
- **Super + Space**         - Open Program Launcher (i.e, Rofi or dmenu)
- **Print**                 - Take Screenshot (Save to $HOME/Pictures/Screenshots directory)
- **Super + Print**         - Take Screenshot (Save to $HOME/Pictures/Git directory)
- **Super + Alt + Print**   - Take Screenshot of the focused window (Save to $Home/Pictures/Screenshots)
- **Super + Shift + Print** - Take Screenshot of a cursor selected area (Save to &Home/Pictures/Screenshots)
- **Super + X**         - Lock Screen
- **Super + Shift + m** - Enter Master Stack Layout Mode (indeed, [DWM's](https://dwm.suckless.org/) Master Stack Layout)
- **Super + Shift + r** - Exit Master Stack Layout Mode


### Music Controls

**Note**: As I use *mpd* and *ncmpcpp* to play songs, I use *[mpc](https://archlinux.org/packages/extra/x86_64/mpc/)* for playback control.

- **Super + Alt + Arrow Up**    - Play Previous Song
- **Super + Alt + Arrow Down**  - Play Next Song
- **Super + Alt + Comma**       - Play Songs
- **Super + Alt + Period**      - Pause Songs
